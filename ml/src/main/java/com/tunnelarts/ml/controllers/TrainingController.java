package com.tunnelarts.ml.controllers;

import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.options;
import static spark.Spark.before;
import static spark.Spark.port;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;

import org.json.simple.JSONObject;

import com.tunnelarts.ml.services.CloudStorageService;
import com.tunnelarts.ml.services.PMMLGeneratorService;
import com.tunnelarts.ml.services.REngineService;

public class TrainingController {

	public TrainingController() {
//		enableCORS("http://localhost:4232", "POST", "Content-Type"); //Dev Env
		enableCORS("http://tunnelarts-subscription.azurewebsites.net", "POST", "Content-Type"); //Prod Env
	}
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		
		port(80);
		
		new TrainingController();
		
		post("/training/:tunnelId/upload-file", (req, res) -> {
			
			String tunnelId = req.params(":tunnelId");
			
			String location = "C:\\model_training\\tunnels\\" + tunnelId;  // the directory location where files will be stored
			
			File f = new File(location);
			if(!f.exists()) { 
				Files.createDirectory(Paths.get(location));
			}
			
			long maxFileSize = 100000000;  // the maximum size allowed for uploaded files
			long maxRequestSize = 100000000;  // the maximum size allowed for multipart/form-data requests
			int fileSizeThreshold = 1024;  // the size threshold after which files will be written to disk
		
			if (req.raw().getAttribute("org.eclipse.jetty.multipartConfig") == null) {
				MultipartConfigElement multipartConfigElement = new MultipartConfigElement(location, maxFileSize, maxRequestSize, fileSizeThreshold);
				 req.raw().setAttribute("org.eclipse.jetty.multipartConfig", multipartConfigElement);
			}
			
			Collection<Part> parts = req.raw().getParts();
			for(Part part : parts) {
				System.out.println("Filename: " + part.getSubmittedFileName());
				System.out.println("Size: " + part.getSize());
			}
			
			Part uploadedFile = req.raw().getPart("0");
			
			Path out = Paths.get(location + "\\history.csv");
			try (final InputStream in = uploadedFile.getInputStream()) {
				Files.copy(in, out);
				uploadedFile.delete();
			}
			
			parts = null;
			uploadedFile = null;
			
			res.type("application/json");
			JSONObject obj = new JSONObject();
		    obj.put("message", "OK");
			
		    return obj.toString();
		});
		
		post("/training/:tunnelId/train-model", (req, res) -> {
			String tunnelId = req.params(":tunnelId");
			REngineService rEngineService = new REngineService(tunnelId);
			rEngineService.run();
			res.type("application/json");
			
			JSONObject obj = new JSONObject();
		    obj.put("message", "OK");
		    obj.put("rmse", rEngineService.getRmse());
		    obj.put("precision", new Integer(0));
			
		    return obj.toString();
		});
		
		post("/training/:tunnelId/build-predictors", (req, res) -> {
			String tunnelId = req.params(":tunnelId");
			PMMLGeneratorService pmmlGeneratorService = new PMMLGeneratorService(tunnelId);
			pmmlGeneratorService.run();
			res.type("application/json");
			JSONObject obj = new JSONObject();
		    obj.put("message", "OK");
		    
		    return obj.toString();
		});
		
		post("/training/:tunnelId/upload2cloud", (req, res) -> {
			String tunnelId = req.params(":tunnelId");
			CloudStorageService cloudStorageService = new CloudStorageService(tunnelId);
			cloudStorageService.run();
			
			deleteAllFiles(tunnelId);
			String location = "C:\\model_training\\tunnels\\" + tunnelId;
			Files.delete(Paths.get(location + "\\history.csv"));
			res.type("application/json");
			JSONObject obj = new JSONObject();
		    obj.put("message", "OK");
			
		    return obj.toString();
		});
	}

	private static void deleteAllFiles(String tunnelId) {
		try {
			Path pmmlFilePath, pmmlFilePathSer;
			String locationPmml = "C:\\model_training\\tunnels\\" + tunnelId + "\\PMML";
			String locationPmmlSer = "C:\\model_training\\tunnels\\" + tunnelId + "\\PMML_SER";
			
			for(int i = 1; i <= 50; i++) {
				pmmlFilePath = Paths.get(locationPmml + "\\PMML_Modelo_Skava_" + i + ".pmml");
				pmmlFilePathSer = Paths.get(locationPmmlSer + "\\PMML_Modelo_Skava_" + i + ".pmml.ser");
				Files.delete(pmmlFilePath);
				Files.delete(pmmlFilePathSer);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void enableCORS(final String origin, final String methods, final String headers) {

	    options("/*", (request, response) -> {

	        String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
	        if (accessControlRequestHeaders != null) {
	            response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
	        }

	        String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
	        if (accessControlRequestMethod != null) {
	            response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
	        }

	        return "OK";
	    });

	    before((request, response) -> {
	        response.header("Access-Control-Allow-Origin", origin);
	        response.header("Access-Control-Request-Method", methods);
	        response.header("Access-Control-Allow-Headers", headers);
	        // Note: this may or may not be necessary in your particular application
	        response.type("application/json");
	    });
	}
}
