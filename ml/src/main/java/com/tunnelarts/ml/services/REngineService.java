package com.tunnelarts.ml.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

public class REngineService {

	private String tunnelId;
	private double rmse;
	
	public REngineService(String tunnelId) {
		this.tunnelId = tunnelId;
	}
	
	public void run() {
		RConnection connection = null;
		
		try {
			connection = new RConnection();
//			String sourcePath = "D:/Metricarts/Proyectos/SKAVA/Development/Utils/R_module/train_model/test-script.R";
			String sourcePath = "C:/model_training/script/trainer.R";
			String csvPath = "C:/model_training/tunnels/" + this.tunnelId + "/history.csv";
			
			String location = "C:\\model_training\\tunnels\\" + this.tunnelId + "\\PMML";
			String destDirPath = "C:/model_training/tunnels/" + this.tunnelId + "/PMML";
			
			File f = new File(location);
			if(!f.exists()) { 
				Files.createDirectory(Paths.get(location));
			}
			
			connection.eval("source('"+ sourcePath +"')");
			
			System.out.println("R Engine connected");
			System.out.println("Training model from CSV file...");
			String[] res = connection.eval("train_models('"+ csvPath +"', '" + destDirPath + "')").asStrings();
			setRmse(new Double(res[0]));
			System.out.println("Training DONE!...");
		} 
		catch (RserveException e) {
          e.printStackTrace();
		} 
		catch (REXPMismatchException e) {
          e.printStackTrace();
		} 
		catch (REngineException e) {
          e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		finally{
          connection.close();
          System.out.println("R Engine disconnected");
		}
	}

	public double getRmse() {
		return rmse;
	}

	private void setRmse(double rmse) {
		this.rmse = rmse;
	}
	
	
}
