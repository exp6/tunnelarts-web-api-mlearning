package com.tunnelarts.ml.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.xml.bind.JAXBException;
import javax.xml.transform.Source;

import org.dmg.pmml.PMML;
import org.jpmml.model.ImportFilter;
import org.jpmml.model.JAXBUtil;
import org.jpmml.model.SerializationUtil;
import org.jpmml.model.visitors.LocatorNullifier;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class PMMLGeneratorService {

	private String tunnelId;
	
	public PMMLGeneratorService(String tunnelId) {
		this.tunnelId = tunnelId;
	}
	
	public void run() {
    	PMML pmml;
    	File serFile, initialFile;
    	
    	String location = "C:\\model_training\\tunnels\\" + this.tunnelId + "\\PMML_SER";
    	String locationPmml = "C:\\model_training\\tunnels\\" + this.tunnelId + "\\PMML";
    	try {
    		File f = new File(location);
			if(!f.exists()) { 
				Files.createDirectory(Paths.get(location));
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
    	
    	System.out.println("Converting PMML files to .pmml.ser files...");
    	for(int i = 1; i <= 50; i++) {
    		serFile = new File(location + "\\PMML_Modelo_Skava_" + i + ".pmml.ser");
    		initialFile = new File(locationPmml + "\\PMML_Modelo_Skava_" + i + ".pmml");
    		
            try(InputStream is = new FileInputStream(initialFile)){
    			Source source = ImportFilter.apply(new InputSource(is));
    			pmml = JAXBUtil.unmarshalPMML(source);
    			
    			LocatorNullifier locatorNullifier = new LocatorNullifier();
    			locatorNullifier.applyTo(pmml);
    			
    			OutputStream os = new FileOutputStream(serFile);
    			SerializationUtil.serializePMML(pmml, os);	
    			
    			os.flush();
    			os.close();
    		} catch (IOException e) {
    			e.printStackTrace();
    		} catch (JAXBException e) {
    			e.printStackTrace();
    		} catch (SAXException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	System.out.println("PMML SER files CREATED!");
	}
}
