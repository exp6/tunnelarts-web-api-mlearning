package com.tunnelarts.ml.services;

import java.io.File;

public class CloudStorageService {

	private StorageClient provider;
	private final String STORAGE_CONNECTION_STRING =
            "DefaultEndpointsProtocol=http;"
                    + "AccountName=tunnelarts;"
                    + "AccountKey=IBPSBXbTCCLdvECvlEz/shJl/ABj91NX6onWKFnuOovApjCdawER8PgeTHtxbHFn58NSk0ZWaoeBIV5tofmGOg==";
	private String tunnelId;
	
	public CloudStorageService(String id) {
		provider = new StorageClient(STORAGE_CONNECTION_STRING);
		tunnelId = id;
	}
	
	public void run() {
		File serFile;
		provider.initBlobContainer("tunnel-prediction-" + tunnelId);
		String location = "C:\\model_training\\tunnels\\" + this.tunnelId + "\\PMML_SER";
		String locationHistory = "C:\\model_training\\tunnels\\" + this.tunnelId + "\\history.csv";
		
		System.out.println("Uploading to Cloud...");
		for(int i = 1; i <= 50; i++) {
//			serFile = new File("D:\\Metricarts\\Proyectos\\SKAVA\\Development\\Utils\\R_module\\train_model\\PMML\\SER\\PMML_Modelo_Skava_" + i + ".pmml.ser");
			serFile = new File(location + "\\PMML_Modelo_Skava_" + i + ".pmml.ser");
			provider.uploadData(serFile);
		}
		
		File csvFile =  new File(locationHistory);
		provider.uploadData(csvFile);
		
		System.out.println("UPLOAD DONE!");
	}
}
