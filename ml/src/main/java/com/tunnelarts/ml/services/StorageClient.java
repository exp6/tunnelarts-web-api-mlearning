package com.tunnelarts.ml.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.BlobContainerPermissions;
import com.microsoft.azure.storage.blob.BlobContainerPublicAccessType;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;

public class StorageClient {
	private String storageConnectionString;
    private CloudBlobClient serviceClient;
    private CloudBlobContainer container;

    public StorageClient(String connectionStr) {
        storageConnectionString = connectionStr;
        createCloudBlobClient();
    }

    private void createCloudBlobClient() {
        try {
            CloudStorageAccount cloudStorageAccount = CloudStorageAccount.parse(storageConnectionString);
            serviceClient = cloudStorageAccount.createCloudBlobClient();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    public void initBlobContainer(String containerId) {
    	try {
			container = serviceClient.getContainerReference(containerId);
			container.createIfNotExists();
	    	BlobContainerPermissions blobContainerPermissions = new BlobContainerPermissions();
	    	blobContainerPermissions.setPublicAccess(BlobContainerPublicAccessType.CONTAINER);
	    	container.uploadPermissions(blobContainerPermissions);
		} catch (URISyntaxException | StorageException e) {
			e.printStackTrace();
		}	
    }
    
    public void uploadData(File sourceFile) {

        final File mSourceFile = sourceFile;
        final String mFileName = sourceFile.getName();
        
        try {
        	CloudBlockBlob blob = container.getBlockBlobReference(mFileName);
        	InputStream input = new FileInputStream(mSourceFile);
        	blob.upload(input, mSourceFile.length());
        	input.close();
        }
        catch (FileNotFoundException fileNotFoundException) {
        	System.out.print("FileNotFoundException encountered: ");
        	System.out.println(fileNotFoundException.getMessage());   
        }
        catch (StorageException storageException) {
        	System.out.print("StorageException encountered: ");
        	System.out.println(storageException.getMessage());
        }
        catch (Exception e) {
        	System.out.print("Exception encountered: ");
        	System.out.println(e.getMessage());
        }
    }
}
