package com.tunnelarts.ml.tests;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBException;
import javax.xml.transform.Source;

import org.dmg.pmml.PMML;
import org.jpmml.model.ImportFilter;
import org.jpmml.model.JAXBUtil;
import org.jpmml.model.SerializationUtil;
import org.jpmml.model.visitors.LocatorNullifier;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class AppService {

	public void run() {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    	
    	PMML pmml;
    	File serFile = new File("D:\\Metricarts\\Proyectos\\SKAVA\\Development\\Utils\\R_module\\train_model\\PMML\\SER\\PMML_Modelo_Skava_1.pmml.ser");
    	
    	try(InputStream is = classLoader.getResourceAsStream("PMML_Modelo_Skava_1.pmml")){
			Source source = ImportFilter.apply(new InputSource(is));
			pmml = JAXBUtil.unmarshalPMML(source);
			
			LocatorNullifier locatorNullifier = new LocatorNullifier();
			locatorNullifier.applyTo(pmml);
			
			OutputStream os = new FileOutputStream(serFile);
			SerializationUtil.serializePMML(pmml, os);	
			
			os.flush();
			os.close();
			System.out.println("PMML SER created!");
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}
}