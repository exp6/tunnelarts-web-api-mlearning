package com.tunnelarts.ml.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.transform.Source;

import org.apache.commons.io.FileUtils;
import org.dmg.pmml.PMML;
import org.dmg.pmml.Visitor;
import org.jpmml.model.ImportFilter;
import org.jpmml.model.JAXBUtil;
import org.jpmml.model.SerializationUtil;
import org.jpmml.model.visitors.LocatorNullifier;
import org.jpmml.model.visitors.LocatorTransformer;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main( String[] args )
    {
    	ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    	
    	PMML pmml;
    	File serFile = new File("D:\\Metricarts\\Proyectos\\SKAVA\\Development\\Utils\\R_module\\train_model\\PMML\\SER\\PMML_Modelo_Skava_2.pmml.ser");
    	
    	try(InputStream is = classLoader.getResourceAsStream("PMML_Modelo_Skava_2.pmml")){
			Source source = ImportFilter.apply(new InputSource(is));
			pmml = JAXBUtil.unmarshalPMML(source);
			
			LocatorNullifier locatorNullifier = new LocatorNullifier();
			locatorNullifier.applyTo(pmml);
			
			OutputStream os = new FileOutputStream(serFile);
			SerializationUtil.serializePMML(pmml, os);	
			
			System.out.println("Done!");
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
    	
    }
}
