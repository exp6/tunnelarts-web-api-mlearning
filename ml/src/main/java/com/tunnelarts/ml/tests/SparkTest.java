package com.tunnelarts.ml.tests;

import static spark.Spark.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBException;
import javax.xml.transform.Source;

import org.dmg.pmml.PMML;
import org.jpmml.model.ImportFilter;
import org.jpmml.model.JAXBUtil;
import org.jpmml.model.SerializationUtil;
import org.jpmml.model.visitors.LocatorNullifier;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class SparkTest {

	public static void main( String[] args ) {
		
		get("/training", (req, res) -> {
			
//			RConnection connection = null;
			
//			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
	    	
//	    	PMML pmml;
//	    	File serFile = new File("D:\\Metricarts\\Proyectos\\SKAVA\\Development\\Utils\\R_module\\train_model\\PMML\\SER\\PMML_Modelo_Skava_2.pmml.ser");
	    	
			try {
//				connection = new RConnection();
//				String sourcePath = "D:/Metricarts/Proyectos/SKAVA/Development/Utils/R_module/train_model/test-script.R";
//				
//				connection.eval("source('"+ sourcePath +"')");
//				
//				System.out.println("Done connection!");
//				
//				double d = connection.eval("sqr(9)").asDouble();
//				System.out.println(d);
				
				byte[] blob = req.bodyAsBytes();
				
				AppService appService = new AppService();
				appService.run();
			} 
//			catch (RserveException e) {
//	          e.printStackTrace();
//			} 
//			catch (REXPMismatchException e) {
//	          e.printStackTrace();
//			} 
//			catch (REngineException e) {
//	          e.printStackTrace();
//			} 
			finally{
//	          connection.close();
			}
			
			return "Everything done correctly!!";
		});
	}
}
