package com.tunnelarts.ml.tests;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

public class RTest {

	public static void main( String[] args ) {
		
		RConnection connection = null;
		
		try {
			connection = new RConnection();
			String sourcePath = "D:/Metricarts/Proyectos/SKAVA/Development/Utils/R_module/train_model/test-script.R";
			connection.eval("source('"+ sourcePath +"')");
			
			System.out.println("Done connection!");
			
			double d = connection.eval("sqr(9)").asDouble();
			System.out.println(d);
		}
		catch (RserveException e) {
          e.printStackTrace();
		} 
		catch (REXPMismatchException e) {
          e.printStackTrace();
		} 
		catch (REngineException e) {
          e.printStackTrace();
		} finally{
          connection.close();
		}
    }
}
